#include "input.h"

bool simple_quit_check(SDL_Event* e) {
	if(e->type == SDL_KEYDOWN) {
		return e->key.keysym.sym == SDLK_q;
	} 
	else {
		return e->type == SDL_QUIT;
	}
}

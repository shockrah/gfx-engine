#ifndef INPUT_H
#define INPUT_H

#include <SDL2/SDL.h>
/*
 * Used to simply check if 'q' has been pressed or not
 * */
bool simple_quit_check(SDL_Event*);
#endif

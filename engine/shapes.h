#ifndef SHAPES_H
#define SHAPES_H
#include "./colors.h"

struct Point {
	int x, y;
	void draw();
};

struct Triangle { 
	int x, y;
	float base;
	float height;
	void draw();
};

struct Rect {
	/* Indicates the bottom left corner of the rectangle from which the rest is drawn */
	int x, y;
	float theta;
	float base;
	float height;
	void draw();
};

struct Ellipse {
	int cx, cy;
	int ex, ey;
	void draw(float angle);
};

struct Line {
	int xs, ys; // start
	int xe, ye; // end cords
	void draw();
};
#endif /* SHAPES_H */

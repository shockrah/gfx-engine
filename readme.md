# GFX Engine

Quick explanation: This engine is essentially a way to interface with SDL in a way that removes some/all of the necessity to remember how to use SDL.
A much simple _canvas_ library is put on top to work at a more conceptual level.

Projects that utilize this engine are placed into their own respective directories. i.e. the project for boids is under `boids/`.

#include "SDL2/SDL.h"
#include "canvas/canvas.h"
#include "cells/cells.h"


int main(void) {
	if(Canvas::init() != CANVAS_INIT_SUCCESS) {
		return 1;
	}
	else {
		main_cell_simulation();
	}

	Canvas::close();
	return 0;
}


#!/bin/sh

# Prelim checks and setting env for later
if [ -z $1 ];then
	echo No module name given
	exit 1
fi

echo Building structure for $1
mkdir -p $1

# Skeleton for the makefile
cat <<EOF > $1/Makefile
cc=g++ -Wall -O2 -lm

../build/NAME.o: NAME.cc
	\$(cc) -c $< -o \$@
EOF

# Now we fix the makefile with the actual module name
sed -i "s/NAME/$1/g" $1/Makefile

# Skeleton for the header file
cdef=`echo $1 | tr '[:lower:]' '[:upper:]'`
cat <<EOF > $1/$1.h
#ifndef ${cdef}_H
#define

#endif /* _H */
EOF


# Skeleton for the .cc file
cat <<EOF > $1/$1.cc
#include "./$1.h"
EOF

/*
 * This module lays out the behavior of a group of cells which are really just 
 * a bunch of squares
 */

#include <stdlib.h>
#include <cassert>
#include <iostream>
#include <SDL2/SDL.h>
#include <math.h>
#include <algorithm>

#include "cells.h"
#include "../canvas/canvas.h"
#include "../input/input.h"
#include "../shapes/shapes.h"
#include "../vec/vec.h"

#define SPACE_HEIGHT WIN_HEIGHT
#define SPACE_WIDTH WIN_WIDTH

#define CELL_RADIUS 10
#define CELL_LOS (CELL_RADIUS + 10) // radial los
#define SWARM_SIZE 25
#define LEADER_ID 0

#define SetColorPassive() SDL_SetRenderDrawColor(Canvas::render, 0x33, 0xD1, 0xFF, 0xff);
#define SetColorActive() SDL_SetRenderDrawColor(Canvas::render, 0xFF, 0, 0, 0xff);

void Cell::init(int x, int y, int id, double velx, double vely, double bx, double by) {
	this->x = x;
	this->y = y;
	this->bias_x = bx;
	this->bias_y = by;
	this->id = id;
	this->vel.x = velx;
	this->vel.y = vely;
	this->cellproc = false;
}

void Cell::draw(void) {
	SDL_Rect r = {this->x, this->y, CELL_RADIUS, CELL_RADIUS};
	if(this->cellproc) {
		SetColorActive();
	}
	else {
		SetColorPassive();
	}
	SDL_RenderFillRect(Canvas::render, &r);
}

double Cell::dist(const Cell* other) {
	int dx = pow((this->x - other->x),2);
	int dy = pow((this->y - other->y),2);
	return sqrt(dx + dy);
}


void main_cell_simulation(void) {
    Cell cells[SWARM_SIZE];

	// velocity vector is randomized so this helps us seed those later
    srand(time(NULL));
	// gives us a reliable random float to move around the screen later on but 
	// without values between 0 and abs(1) since those lock us on that axis
	auto random_double = [](double min, double max) { 
		assert(min < max);
		const double seed = ((double)rand()) / ((float) RAND_MAX);
		const double range = max - min;
		double ret = (seed * range) + min;
		if(ret < 0.0 && ret > -1.0) {
			return ret - 1.0;
		}
		if(ret > 0.0 && ret < 1.0) {
			return ret + 1.0;
		}
		return ret;
	};
    for(int i = 0; i < SWARM_SIZE; i++ ) {
        int x = rand() % (SPACE_WIDTH - CELL_LOS);
        int y = rand() % (SPACE_HEIGHT - CELL_LOS);

		cells[i].init(x, y, i, 
				random_double(-2.0, 2.0), random_double(-2.0, 2.0),
				random_double(0.0, M_PI/2), random_double(0.0, M_PI/2));
    }

    for(auto c : cells) {
        c.show_pos();
    }

    while(Input::pass_quit_check()) {
        Canvas::clear();
		for(auto &c: cells) {
			//c.non_collide(cells); // ignore coliisions for now
			c.follow(cells);
			c.drift();
			c.draw();
		}
        Canvas::update(10);
    }
}

static int _direction(Point& a, Point& b, Point& c) {
	// Tells us if 3 points are colinear, clockwise, or counter-clockwise orienteated
	// NOTE: intuitively this is magic so just look up this formulae and 
	// deal with the consequences later
	int val = (b.y-a.y)*(c.x-b.x) - (b.x-a.x)*(c.y-b.y);
	if(val == 0) {
		return 0; // co-linear
	}
	else if(val < 0) {
		return 1; // clockwise (i think)
	}
	else {
		return 2; // counterclock wise (i think)
	}
}

bool _colinear(Point& a, Point& b, Point& c) {
	// Tells us if 3 points are colinear or not
	bool cx_max = (c.x <= std::max(a.x, b.x));
	bool cx_min = (c.x <= std::min(a.x, b.x));
	bool cy_max = (c.y <= std::max(a.y, b.y));
	bool cy_min = (c.y <= std::min(a.y, b.y));
	return cx_max && cx_min && cy_max && cy_min;
}

static bool confirm_collision(int d1, int d2, int d3, int d4, Point& p1, Point& p2, Point& p3, Point& p4) {
	if(d1 != d2 && d3 != d4) {
		return true;
	}
	if( d1 == 0 && _colinear(p1, p2, p3) ){
		return true;
	}
	if( d2 == 0 && _colinear(p1, p2, p4) ){
		return true;
	}
	if( d3 == 0 && _colinear(p3, p4, p1) ){
		return true;
	}
	if( d4 == 0 && _colinear(p3, p4, p2) ){
		return true;
	}
	return false;
}

void Cell::set_avoid_angle(const Cell* other) {
	Point p1 = Point {this->x, this->y};
	Point p2 = Point {this->x + (int)vel.x, this->y + (int)vel.y};
	Point p3 = Point {other->x, this->y};
	Point p4 = Point {other->x + (int)vel.x, this->y + (int)vel.y};

	int d1 = _direction(p1, p2, p3);
	int d2 = _direction(p1, p2, p4);
	int d3 = _direction(p3, p4, p1);
	int d4 = _direction(p3, p4, p2);

	if(confirm_collision(d1, d2, d3, d4, p1,p2,p3,p4)) {
		// aim for the other cells current position which on next frame
		// will be the previous position
		// in this way we will trail other cells 
		double x_fix = (other->x-this->x == 0) ? 1.0 : (double)(other->x-this->x);
		double new_angle = atan( (other->y-this->y) / x_fix/* avoiding div by 0 errors*/ );
		double mag = sqrt((this->vel.x*this->vel.x) + (this->vel.y*this->vel.y));

		double vel_y = mag * sin(new_angle/*+ this->bias_y*/);
		double vel_x = mag * cos(new_angle/*+ this->bias_x*/);

		this->vel.x = vel_x;
		this->vel.y = vel_y;
	}
}

void Cell::non_collide(const Cell members[]) {
	/*
	 * Check if we we're close to anyone else if we are 
	 * 1. change our color via the self.cellproc flag
	 * 2. Course correct so we don't collide, if we even need to
	 * 	When we course correct we want to make the smallest possible change
	 * 	Find the position vectors tanent to the opposing cells
	 * */
	bool close = false;
	double closest_dist = 10000.0;
	for(size_t i = 0; i < SWARM_SIZE; i++) {
		double distance = this->dist(&members[i]);
		if(distance < (double)CELL_LOS && this->id != members[i].id) {
			this->set_avoid_angle(&members[i]);
			close = true;
		}
	}
	this->cellproc = close;
}


void Cell::follow(Cell members[]) {
	Cell* closeby[SWARM_SIZE] = {NULL};
	// slow method first
	for(size_t i = 0; i<SWARM_SIZE; i++) {
		if(this->dist(&members[i]) < CELL_LOS * 2.5) {
			closeby[i] = &members[i];
		}
	}
	// next average all coords
	Point p_mean = Point {0,0};
	int cc = 0;
	for(auto c : closeby) {
		if(c) {
			p_mean.x += c->x;
			p_mean.y += c->y;
			cc++;
		}
	}
	SDL_Log("%d -> %d, %d", this->id, p_mean.x/3, p_mean.y/3);
	// Derive new target angle
	int x = p_mean.x - this->x;
	// keep everything within reasonable bounds lest we go to space
	if(x == 0 || p_mean.x > SPACE_WIDTH || p_mean.x < 0 || 
			p_mean.y > (SPACE_HEIGHT-CELL_RADIUS) || p_mean.y < 0) { return; }

	int y = p_mean.y - this->y;
	double angle = atan(y/x);

	double mag = sqrt((this->vel.x*this->vel.x) + (this->vel.y*this->vel.y));

	int nvx = cos(mag*angle);
	int nvy = sin(mag*angle);

	this->vel.x = nvx;
	this->vel.y = nvy;
}

void Cell::drift(void) {
	int next_x = this->x + (int)vel.x;
	int next_y = this->y + (int)vel.y;

	if(next_x > (SPACE_WIDTH - CELL_RADIUS) || next_x < 0) {
		this->vel.x *= -1.0;
	}

	if(next_y > (SPACE_HEIGHT - CELL_RADIUS) || next_y < 0) {
		this->vel.y *= -1.0;
	}

	this->x = next_x;
	this->y = next_y;
}

void Cell::show_pos(void) {
    std::cout << this->id << ": (" << this->x << "," << this->y << ")@"
		<< this->vel.x << " " << this->vel.y << "\n";
}

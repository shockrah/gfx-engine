#ifndef CELLS_H
#define CELLS_H
#include "../vec/vec.h"

struct Cell {
	int x, y;
	int id;
	bool cellproc; // tells us if we're near another cell or not
	double bias_x, bias_y; // direction this cell prefers to move in to avoid collisions
	VelocityVec2D vel;
public:
	void init(int x, int y, int id, double velx, double vely, double bx, double by);
	void draw(void);
	// distance to another cell
	double dist(const Cell* other);
	void set_avoid_angle(const Cell* other);
	// stops a cell from bumping into others
	void non_collide(const Cell[]);
	void follow(Cell[]);
	void drift(void);
	void show_pos(void);
};


void main_cell_simulation(void);

#endif /* CELLS_H */

#ifndef GRAPH_ROUTING_H
#define GRAPH_ROUTING_H

#include <vector>
#include <tuple>
#include <string>

using std::vector;
using std::tuple;

class Node {
public:
	unsigned id;
	vector<tuple<Node*, unsigned>> links;

	Node(unsigned id);
	bool operator==(const Node& other);
	void remove_link(unsigned id);
};


class Graph {
public:
	std::vector<Node*> members;
	Graph(unsigned member_count);
	void add_node(void);
	void remove_node(unsigned id);
	void conn_table(void);
	void hook(unsigned, unsigned);
	void random_conns(void);
};

#endif /* _H */

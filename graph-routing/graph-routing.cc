#include <iostream>
#include <string>
#include <vector>
#include <tuple>
#include <functional>
#include <optional>
#include <time.h> /* used for rng seeding */
#include <cstdlib>


#include "./graph-routing.h"
#include "../engine/canvas.h"

using std::tuple;
using std::vector;

Node::Node(unsigned id) {
	this->id = id;
	this->links = std::vector<std::tuple<Node*, unsigned>>();
}

bool Node::operator==(const Node& other) {
	return this->id == other.id;
}
void Node::remove_link(unsigned id) {
	if(id >= this->links.size()) { return; }

	this->links.erase(this->links.begin()+id);
}

Graph::Graph(unsigned members) {
	// For now we give each member a random location on the map
	// With a random chance to have a connection to another member in the graph
	for(size_t i = 0; i < members; i++) {
		Node* child = new Node(i);
		this->members.push_back(child);
	}
}


void Graph::add_node(void) {
	unsigned id = this->members.size();
	Node* node = new Node(id);
	this->members.push_back(node);
}

void Graph::remove_node(unsigned id) {
	// Make other members "forget" this member
	for(auto i: this->members) {
		i->remove_link(id);
	}

	// Finally we can remove the target link 
	this->members.erase(this->members.begin()+id);
}

void Graph::conn_table(void) {
	for(auto member: this->members) {
		std::cout << "member-" << member->id << "\n";
		std::cout << "\tLinks: ";
		for(auto link : member->links) {
			std::cout << std::get<1>(link) << ", ";
		}
		std::cout << "\n";
	}
}

void Graph::hook(unsigned src_id, unsigned dest_id) {
	// Some safety for silly mistakes
	if(src_id == dest_id) { return; }
	if(src_id >= this->members.size() || dest_id >= this->members.size()) { return; }

	// Hold a reference so the target nodes
	auto src = this->members[src_id];
	auto dest = this->members[dest_id];
	// Last sanity check
	if(src == NULL || dest == NULL) { return; }

	// Ensure we don't somehow add the same thing twice
	for(auto n : src->links) {
		unsigned id = std::get<1>(n);
		if(id == dest_id) { return; }
	}
	src->links.push_back(std::make_tuple(dest, dest_id));
}

void Graph::random_conns(void) {
	/*
	 * This method basically creates random connections for each node
	 * We do this to create a kind of connected network
	 */
	srand(time(NULL)); /* seed rng engine */
	/* max connections any 1 node is allowed to have */
	const unsigned max_conns = this->members.size() - 2; // ub w/ graph's of size 0
	std::cout << "Max connections per node: " << max_conns << "\n";
	for(unsigned i = 0; i < this->members.size(); i++) {
		// Determine what our max conns are for that connection
		for(unsigned p = 0; p < max_conns; p++ ) {
			/* Percent chance to create a new connection
			 * This should hopefully give us more interesting topologies
			 */
			const bool create = (rand() % 100) >= 10;
			if(create == false) { 
				std::cout << "Not creating a connection\n";
				continue; 
			}
			/* 
			 * Picking a random node here to see who our partner is 
			 * We take care to not pick ourselves as well
			 * */
			unsigned target = p;
			while (target == this->members[i]->id) {
				target = rand() % max_conns;
			}
			this->hook(i, target);
		}
	}
}

int main(void) {
	/*
	 * Left click 	-> add a new node to the graph at that location
	 * Right click 	-> select a node to start a link from
	 * 	Second right click connects the two nodes that we want to connect
	 * */
	//if(Canvas::init() != CANVAS_INIT_SUCCESS) {
	//	return 1;
	//}
	Graph* net = new Graph(5);
	net->random_conns();
	//net->hook(3,4); net->hook(4,3);

	std::cout << "Member count: " << net->members.size() << "\n";
	net->conn_table();

	//Canvas::cleanup();
	return 0;
}

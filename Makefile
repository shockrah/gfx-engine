# General current project things
cc=g++ -Wall -O2 -Werror
libs=-lm -lSDL2
output_dir=bin
engine_dir=$(output_dir)/engine

# ===================================
# Build projects
# ===================================

graph-routing: $(engine_dir)/canvas.o $(engine_dir)/shapes.o $(engine_dir)/input.o
	make --no-print-directory -C $@
	$(cc) $@/$@.cc $^ -o $(output_dir)/$@.bin -lSDL2



boids: $(engine_dir)/canvas.o $(output_dir)/cells.o
	make --no-print-directory -C $@
	$(cc) $@/$@.cc $^ -o $(output_dir)/$@.bin -lSDL2

# ===================================
# Engine Components
# ===================================

$(engine_dir)/%.o: engine/%.cc
	$(cc) -c $< -o $@ $(libs)


# ===================================
# Detachable modules
# ===================================
bin/cells.o: cells/cells.cc
	$(cc) -c $< -o $@ $(libs)


# ===================================
# Setup for the sake of convenience
# ===================================
setup:
	mkdir -p $(output_dir) $(engine_dir)

# ===================================
# GENERAL CLEAN OUT
# ===================================
clean:
	rm -rf $(output_dir)/*

.PHONY: boids graph

